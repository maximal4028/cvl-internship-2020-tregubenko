#!/usr/bin/env python3
# -*- coding: utf-8 -*
"""
Images preprocessing. Create classes diagram.
Create separate RGBY image with target labels.
"""
import os
import cv2
import argparse

import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

# target names
subcell_locs = {
    0: "Nucleoplasm",
    1: "Nuclear membrane",
    2: "Nucleoli",
    3: "Nucleoli fibrillar center",
    4: "Nuclear speckles",
    5: "Nuclear bodies",
    6: "Endoplasmic reticulum",
    7: "Golgi apparatus",
    8: "Peroxisomes",
    9: "Endosomes",
    10: "Lysosomes",
    11: "Intermediate filaments",
    12: "Actin filaments",
    13: "Focal adhesion sites",
    14: "Microtubules",
    15: "Microtubule ends",
    16: "Cytokinetic bridge",
    17: "Mitotic spindle",
    18: "Microtubule organizing center",
    19: "Centrosome",
    20: "Lipid droplets",
    21: "Plasma membrane",
    22: "Cell junctions",
    23: "Mitochondria",
    24: "Aggresome",
    25: "Cytosol",
    26: "Cytoplasmic bodies",
    27: "Rods & rings"
}


def get_args():
    """Arguments parser."""
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('--train', required=True,
                        help='train.csv')
    parser.add_argument('--images', required=True,
                        help='Input dir with image')
    parser.add_argument('--output', required=True,
                        help='Output dir')

    return parser.parse_args()


def get_diagram(data):
    """Create classes diagram

    Parameters
    ----------
    data : Pandas dataframe
        Dataframe with train data.

    """

    labels_num = [value.split() for value in data['Target']]
    labels_num_flat = list(
        map(int, [item for sublist in labels_num for item in sublist]))
    labels = ["" for _ in range(len(labels_num_flat))]
    for i in range(len(labels_num_flat)):
        labels[i] = subcell_locs[labels_num_flat[i]]

    fig, ax = plt.subplots(figsize=(15, 8))
    ax.set_title("Diagram", fontsize=18)
    pd.Series(labels).value_counts().plot(kind='bar', fontsize=15)
    plt.show()


def processing_images(data, input_images, output):
    """Preprocessing images. Create separate RGBY image with target labels.

    Parameters
    ----------
    data : Pandas dataframe
        Train.csv file.
    input_images : str, path object or file-like object
        Path to directory with train images.
    output : str, path object or file-like object
        Path to directory with preprocessing images.

    """

    os.chdir(input_images)

    # get random number from dataset
    im_num = np.random.permutation(len(data['Target']))[0]

    # get title coordinates
    title_ordinate = -650
    title_abscissa = -145
    title_ordinate_sep = 0

    # reset seaborn style
    sns.reset_orig()

    # get image id
    im_id = data.loc[im_num, "Id"]

    # get each image channel as a greyscale image (second argument 0 in imread)
    green = cv2.imread('{}_green.png'.format(im_id), 0)
    red = cv2.imread('{}_red.png'.format(im_id), 0)
    blue = cv2.imread('{}_blue.png'.format(im_id), 0)
    yellow = cv2.imread('{}_yellow.png'.format(im_id), 0)

    # display each channel separately
    fig, ax = plt.subplots(nrows=2, ncols=2, figsize=(15, 15))

    for location in data.loc[im_num, "Target"].split():
        plt.text(title_abscissa, title_ordinate + title_ordinate_sep,
                 subcell_locs[int(location)], fontsize=18)
        title_ordinate_sep -= 35

    # create custom color maps
    cdict1 = {'red': ((0.0, 0.0, 0.0),
                      (1.0, 0.0, 0.0)),

              'green': ((0.0, 0.0, 0.0),
                        (0.75, 1.0, 1.0),
                        (1.0, 1.0, 1.0)),

              'blue': ((0.0, 0.0, 0.0),
                       (1.0, 0.0, 0.0))}

    cdict2 = {'red': ((0.0, 0.0, 0.0),
                      (0.75, 1.0, 1.0),
                      (1.0, 1.0, 1.0)),

              'green': ((0.0, 0.0, 0.0),
                        (1.0, 0.0, 0.0)),

              'blue': ((0.0, 0.0, 0.0),
                       (1.0, 0.0, 0.0))}

    cdict3 = {'red': ((0.0, 0.0, 0.0),
                      (1.0, 0.0, 0.0)),

              'green': ((0.0, 0.0, 0.0),
                        (1.0, 0.0, 0.0)),

              'blue': ((0.0, 0.0, 0.0),
                       (0.75, 1.0, 1.0),
                       (1.0, 1.0, 1.0))}

    cdict4 = {'red': ((0.0, 0.0, 0.0),
                      (0.75, 1.0, 1.0),
                      (1.0, 1.0, 1.0)),

              'green': ((0.0, 0.0, 0.0),
                        (0.75, 1.0, 1.0),
                        (1.0, 1.0, 1.0)),

              'blue': ((0.0, 0.0, 0.0),
                       (1.0, 0.0, 0.0))}

    plt.register_cmap(name='greens', data=cdict1)
    plt.register_cmap(name='reds', data=cdict2)
    plt.register_cmap(name='blues', data=cdict3)
    plt.register_cmap(name='yellows', data=cdict4)

    # show images
    ax[0, 0].imshow(green, cmap="greens")
    ax[0, 0].set_title("Green", fontsize=18)
    ax[0, 1].imshow(red, cmap="reds")
    ax[0, 1].set_title("Red", fontsize=18)
    ax[1, 0].imshow(blue, cmap="blues")
    ax[1, 0].set_title("Blue", fontsize=18)
    ax[1, 1].imshow(yellow, cmap="yellows")
    ax[1, 1].set_title("Yellow", fontsize=18)
    for i in range(2):
        for j in range(2):
            ax[i, j].set_xticklabels([])
            ax[i, j].set_yticklabels([])
            ax[i, j].tick_params(left=False, bottom=False)

    # save images
    plt.savefig(output + '/exmaple_' + str(len(os.listdir(output))))


def main():
    """Application entry point."""
    args = get_args()
    train = pd.read_csv(args.train)
    get_diagram(train)
    processing_images(train, args.images, args.output)


if __name__ == '__main__':
    main()
