#!/usr/bin/env python3
# -*- coding: utf-8 -*
"""
Calculate precision, reacll, f-score
"""
import argparse

import pandas as pd

from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.metrics import recall_score, precision_score, f1_score


def get_args():
    """Arguments parser."""
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('--target', required=True,
                        help='csv with targets')
    parser.add_argument('--predict', required=True,
                        help='csv with predicts')

    return parser.parse_args()


def f1_precision_recall(y_true, y_pred):
    """Calculate f1-score, precision and recall for multilable clasification.

    Parameters
    ----------
    y_true : Pandas dataframe
        Dataframe with targets.
    y_pred : Pandas dataframe
        Dataframe with predicts.
    Returns
    -------
    precision : Float
        Precision value.
    recall : Float
        Recall value.
    f1 : Float
        F1-score value.

    """

    # One Hot Encoding
    multibinarizer = MultiLabelBinarizer()
    y_true_multi = multibinarizer.fit(y_true).transform(y_true)
    y_pred_multi = multibinarizer.transform(y_pred)

    # Calculate precision, recall, f1-score
    precision = precision_score(y_true_multi,
                                y_pred_multi,
                                average='macro',
                                zero_division=0)

    recall = recall_score(y_true_multi,
                          y_pred_multi,
                          average='macro',
                          zero_division=0)

    f1 = f1_score(y_true_multi,
                  y_pred_multi,
                  average='macro',
                  zero_division=0)

    return precision, recall, f1


def main():
    """Application entry point."""
    args = get_args()
    train = pd.read_csv(args.target)
    submission = pd.read_csv(args.predict)
    y_pred = submission['Predicted'].astype(str)
    y_true = train['Target']
    precision, recall, f1 = f1_precision_recall(y_true, y_pred)
    print('Precision =', precision)
    print('Recall =', recall)
    print('F1 =', f1)


if __name__ == '__main__':
    main()
