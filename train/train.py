#!/usr/bin/env python3
# -*- coding: utf-8 -*
"""
Training model (ResNet18)
"""

import os
import torch
import argparse
import torchvision

import numpy as np
import pandas as pd
import torch.optim as optim
import albumentations as albu
import matplotlib.pyplot as plt
import albumentations.pytorch as albupy

from tqdm import tqdm
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter
from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score

from focal_loss import FocalLoss
from human_protein_dataset import HumanProteinDataset


import warnings
warnings.filterwarnings("ignore")
plt.ion()


def get_args():
    """Arguments parser."""
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('--bs', type=int, default=8,
                        help='Batch size.')
    parser.add_argument('--save-model', required=True,
                        help='Path to save model dir.')
    parser.add_argument('--save-board', required=True,
                        help='Path to save tensorboard logs dir.')
    parser.add_argument('--datasets', required=True,
                        help='Path to datasets dir.')
    parser.add_argument('--epochs', type=int, default=10,
                        help='Number of epochs.')
    parser.add_argument('--shape', nargs=2, type=int, default=(224, 224),
                        help='Image shape')
    parser.add_argument('--lr', type=float, default=0.001,
                        help='Learning rate')
    parser.add_argument('--load-model', default='None',
                        help='Path to load model. File with .pth')
    return parser.parse_args()


def split(data):
    """Splitting data.

    Parameters
    ----------
    data : Pandas dataframe
        Train dataframe.

    Returns
    -------
    train_split : Pandas dataframe
        Dataframe with train data.
    test_split : Pandas dataframe
        Dataframe with test data.

    """
    data = pd.read_csv(data)

    # Splitting data
    train_ids, test_ids, train_targets, test_target = train_test_split(
        data['Id'], data['Target'], test_size=0.25, random_state=42)

    # Create 2 dataframes with image's indexes and labels
    train_split = {'Id': train_ids, 'Target': train_targets}
    test_split = {'Id': test_ids, 'Target': test_target}
    train_split = pd.DataFrame(data=train_split)
    test_split = pd.DataFrame(data=test_split)

    return train_split, test_split


def create_model():
    """Create Resnet34 for 28 classes.

    Returns
    -------
    model : pytorch model
        Pretrained ResNet34 for 28 classes

    """
    # Load pretrained model
    model = torchvision.models.resnet34(pretrained=True)

    # Save pretrained weights
    weights = model.state_dict()

    # Load model with 28 outputs
    model = torchvision.models.resnet34(num_classes=28)

    # Delete 2 excess weights
    del weights['fc.bias']
    del weights['fc.weight']

    # Load pretrained weights
    model.load_state_dict(weights, strict=False)

    return model


def main():
    """Application entry point."""
    args = get_args()
    os.chdir(args.datasets)

    # Splitting data
    train_split, test_split = split('train.csv')

    # Create datasets and dataloaders
    train_dataset = HumanProteinDataset(
        data=train_split,
        root_dir='train/',
        transform=albu.Compose([
            albu.Resize(*args.shape),
            albu.RandomRotate90(),
            albu.Flip(),
            albu.VerticalFlip(),
            albu.HorizontalFlip(),
            albu.RandomBrightnessContrast(),
            albu.Normalize(mean=[0.485, 0.456, 0.406],
                           std=[0.229, 0.224, 0.225]),
            albupy.transforms.ToTensor()
        ]))

    test_dataset = HumanProteinDataset(
        data=test_split,
        root_dir='train/',
        transform=albu.Compose([
            albu.Resize(*args.shape),
            albu.Normalize(mean=[0.485, 0.456, 0.406],
                           std=[0.229, 0.224, 0.225]),
            albupy.transforms.ToTensor()
        ]))

    dl_train = DataLoader(train_dataset, batch_size=args.bs, shuffle=True,
                          num_workers=12)
    dl_test = DataLoader(test_dataset, batch_size=args.bs, shuffle=False,
                         num_workers=12)

    # Create model
    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
    model = create_model().to(device)
    optimizer = optim.Adam(model.parameters(), lr=args.lr)
    criterion = FocalLoss()

    # Load model for continue training
    if args.load_model != 'None':
        checkpoint = torch.load(args.load_model)
        model.load_state_dict(checkpoint['model_state_dict'])
        optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
        load_epoch = checkpoint['epoch'] + 1
        loss = checkpoint['loss']

    # Path to TensorBoardX saves
    writer = SummaryWriter(args.save_board)

    # Variable for saving minimal loss
    min_loss = np.inf

    # Loop over the dataset multiple times
    for epoch in range(args.epochs):
        if args.load_model != 'None':
            epoch += load_epoch
        running_loss_train = 0.0
        running_loss_test = 0.0

        # Array for all epoch outputs and labels
        epoch_outputs = np.zeros(28)
        epoch_labels = np.zeros(28)

        # Range for thresholds
        ths = np.arange(-1.2, 1.0, 0.1)

        # Array for calculating all classes f1-score
        scores = np.zeros((len(ths), 28))

        model.train()
        print('Epoch: ', epoch)

        # Train loop
        for batch in tqdm(dl_train, desc='Training...'):
            images, labels = batch[0], batch[1]
            images = images.to(device)
            labels = labels.to(device)

            # Zero the parameter gradients
            optimizer.zero_grad()

            # Forward + backward + optimize
            outputs = model(images)
            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()

            # Save outputs and labels
            for b in range(len(images)):
                epoch_outputs = np.vstack((
                    epoch_outputs,
                    outputs[b].detach().cpu().numpy()))

                epoch_labels = np.vstack((
                    epoch_labels,
                    labels[b].detach().cpu().numpy()))

            # Summary loss for each train batch
            running_loss_train += loss.item()

        # Averange loss for train epoch
        running_loss_train /= len(dl_train)

        # Transorm arrays for finding thresholds
        epoch_outputs = np.delete(epoch_outputs, 0, 0)
        epoch_labels = np.delete(epoch_labels, 0, 0)
        epoch_outputs = epoch_outputs.T
        epoch_labels = epoch_labels.T

        # Find thresholds
        for idx, th in enumerate(ths):
            for jdx, (cls, lab) in enumerate(
                    zip(epoch_outputs, epoch_labels)):
                preds = cls >= th

                score = f1_score(lab, preds, zero_division=0)
                scores[idx, jdx] = score

        best_ths = ths[scores.argmax(axis=0)]

        # Start test loop. Don't save gradients
        model.eval()
        with torch.no_grad():
            for batch in tqdm(dl_test, desc='Testing...'):
                images, labels = batch[0], batch[1]
                images = images.to(device)
                labels = labels.to(device)

                # Only forward
                outputs_test = model(images)
                loss_test = criterion(outputs_test, labels)

                # Summary loss for each test batch
                running_loss_test += loss_test.item()

        # Averange loss for test epoch
        running_loss_test = running_loss_test / len(dl_test)

        # Create tensorboard graphics
        writer.add_scalars('Loss', {'train': running_loss_train,
                                    'test': running_loss_test}, epoch)

        # Save graphics now
        writer.flush()

        # Save model with minimal test loss
        if min_loss > running_loss_test:
            min_loss = running_loss_test
            torch.save({
                'epoch': epoch,
                'model_state_dict': model.state_dict(),
                'optimizer_state_dict': optimizer.state_dict(),
                'loss': min_loss,
                'arguments': args,
                'tresholds': best_ths
            }, os.path.join(args.save_model, 'best_loss.pth'))

        # Save current epoch model
        torch.save({
            'epoch': epoch,
            'model_state_dict': model.state_dict(),
            'optimizer_state_dict': optimizer.state_dict(),
            'loss': min_loss,
            'arguments': args,
            'tresholds': best_ths
        }, os.path.join(args.save_model, 'current_loss.pth'))


if __name__ == '__main__':
    main()
