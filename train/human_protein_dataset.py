#!/usr/bin/env python3
# -*- coding: utf-8 -*
"""
Custom dataset for neural network
"""

import os
import cv2
import torch
import torchvision

import numpy as np
import torch.nn.functional as F

from torch.utils.data import Dataset


class HumanProteinDataset(Dataset):
    """Human Protein dataset.

    Attributes
    ----------
    transform : torchvision.transforms
        Custom transforms.
    sample['image'] : numpy array
        RGB image array.

    """
    def __init__(self, data, root_dir, transform=None):
        """

        Parameters
        ----------
        data : Pandas dataframe
            Dataframe with train data.
        root_dir : string
            Path to train images.
        transform : torchvision.transforms.Compose
            Custom transforms.
        """

        self.data = data
        self.root_dir = root_dir
        self.transform = transform
        self.dataset_info = []

        # Create dictionary with dataset info
        for name, labels in zip(self.data['Id'],
                                self.data['Target'].str.split(' ')):
            self.dataset_info.append({
                'path': os.path.join(self.root_dir, name),
                'labels': np.array([int(label) for label in labels])})
        self.dataset_info = np.array(self.dataset_info)

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        """Get 1 image from dataset.

        Parameters
        ----------
        idx : int
            Image index.

        Returns
        -------
        sample['image'] : numpy array
            RGB image.
        sample['labels'] : numpy array
            Image's labels.
        """

        # Load image
        image = self.load_image(
            self.dataset_info[idx]['path']
        )

        # Load labels
        labels = self.dataset_info[idx]['labels'].astype(np.int64)

        # One-hot encoding labels
        labels = F.one_hot(torch.from_numpy(labels), 28)
        labels = labels.sum(axis=0)
        labels = labels.numpy().astype(np.int64)

        # Create dictionary
        sample = {'image': image, 'labels': labels}

        if self.transform:
            sample = self.transform(**sample)

        return sample['image'], sample['labels']

    @staticmethod
    def load_image(path):
        """Create RGB image from 4 channels.

        Parameters
        ----------
        path : str
            Image's name.

        Returns
        -------
        image : numpy array
            RGB image. Image.shape = (512,512,3,3).

        """
        r = cv2.imread(path + '_red.png', cv2.IMREAD_GRAYSCALE)
        g = cv2.imread(path + '_green.png', cv2.IMREAD_GRAYSCALE)
        b = cv2.imread(path + '_blue.png', cv2.IMREAD_GRAYSCALE)

        image = np.dstack((
            r,
            g,
            b)
        )

        return image
