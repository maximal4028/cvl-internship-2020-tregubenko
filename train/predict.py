#!/usr/bin/env python3
# -*- coding: utf-8 -*
"""
Submit model predicts.
"""
import os
import torch
import argparse

import pandas as pd
import albumentations as albu
import albumentations.pytorch as albupy

from tqdm import tqdm

from train import create_model
from human_protein_dataset import HumanProteinDataset


def get_args():
    """Arguments parser."""
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('--model', required=True,
                        help='Path to your model.')
    parser.add_argument('--dataset', required=True,
                        help='Path to your dataset csv.')
    parser.add_argument('--save-to', required=True,
                        help='Path to save your submission (with filename).')
    parser.add_argument('--images', required=True,
                        help='Path to your images lib.')
    parser.add_argument('--shape', nargs=2, type=int, default=(224,224),
                        help='Image shape')

    return parser.parse_args()


def main():
    """Application entry point."""
    args = get_args()

    # Open dataset with image's names
    submit = pd.read_csv(args.dataset)

    # Create dict for prediction saves
    predicted = []

    # Load model
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    model = create_model()
    checkpoint = torch.load(args.model)
    model.load_state_dict(checkpoint['model_state_dict'])
    ths = checkpoint['tresholds']
    model.to(device)
    model.eval()

    for name in tqdm(submit['Id']):
        # Take an image name
        path = os.path.join(args.images, name)

        # Load image
        image = HumanProteinDataset.load_image(path)

        # Transform image for model
        transform = albu.Compose([
            albu.Resize(*args.shape),
            albu.Normalize(mean=[0.485, 0.456, 0.406],
                           std=[0.229, 0.224, 0.225]),
            albupy.transforms.ToTensor()])
        image = transform(image=image)['image'].unsqueeze(axis=0)

        image = image.to(device)

        output = model(image)

        label_predict = []

        # Save predicts
        for idx in range(28):
            if output[0][idx] >= ths[idx]:
                label_predict.append(idx)

        if label_predict == []:
            label_predict = [0]

        # Make a kaggle submit format
        str_predict_label = ' '.join(str(label) for label in label_predict)
        predicted.append(str_predict_label)

    # Create submit dataframe
    if submit.columns[1] == 'Target':
        submit['Target'] = predicted
        submit = submit.rename({'Target': 'Predicted'}, axis=1)
    else:
        submit['Predicted'] = predicted

    submit.to_csv(args.save_to, index=False)


if __name__ == '__main__':
    main()
