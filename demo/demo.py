#!/usr/bin/env python3
# -*- coding: utf-8 -*
"""
Predict your image
"""
import os
import cv2
import torch
import argparse
import torchvision

import numpy as np
import seaborn as sns
import albumentations as albu
import matplotlib.pyplot as plt
import albumentations.pytorch as albupy

from PIL import Image


subcell_locs = {
    0: "Nucleoplasm",
    1: "Nuclear membrane",
    2: "Nucleoli",
    3: "Nucleoli fibrillar center",
    4: "Nuclear speckles",
    5: "Nuclear bodies",
    6: "Endoplasmic reticulum",
    7: "Golgi apparatus",
    8: "Peroxisomes",
    9: "Endosomes",
    10: "Lysosomes",
    11: "Intermediate filaments",
    12: "Actin filaments",
    13: "Focal adhesion sites",
    14: "Microtubules",
    15: "Microtubule ends",
    16: "Cytokinetic bridge",
    17: "Mitotic spindle",
    18: "Microtubule organizing center",
    19: "Centrosome",
    20: "Lipid droplets",
    21: "Plasma membrane",
    22: "Cell junctions",
    23: "Mitochondria",
    24: "Aggresome",
    25: "Cytosol",
    26: "Cytoplasmic bodies",
    27: "Rods & rings"
}


def get_args():
    """Arguments parser."""
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('--model', required=True,
                        help='Path to your model.')
    parser.add_argument('--image-dir', required=True,
                        help='Input dir with image')
    parser.add_argument('--image-id', required=True,
                        help='Image id (name without "_red.png")')
    parser.add_argument('--save-to', required=True,
                        help='Path to save output images dir')
    parser.add_argument('--shape', nargs=2, type=int, default=(224,224),
                        help='Image shape')

    return parser.parse_args()


def create_model():
    """Create Resnet34 for 28 classes.

    Returns
    -------
    model : pytorch model
        Pretrained ResNet34 for 28 classes

    """
    # Load pretrained model
    model = torchvision.models.resnet34(pretrained=True)

    # Save pretrained weights
    weights = model.state_dict()

    # Load model with 28 outputs
    model = torchvision.models.resnet34(num_classes=28)

    # Delete 2 excess weights
    del weights['fc.bias']
    del weights['fc.weight']

    # Load pretrained weights
    model.load_state_dict(weights, strict=False)

    return model


def load_image(path):
    """Create RGB image from 4 channels.

    Parameters
    ----------
    path : str
        Image's name.

    Returns
    -------
    image : numpy array
        RGB image. Image.shape = (512,512,3,3).

    """
    r = cv2.imread(path + '_red.png', cv2.IMREAD_GRAYSCALE)
    g = cv2.imread(path + '_green.png', cv2.IMREAD_GRAYSCALE)
    b = cv2.imread(path + '_blue.png', cv2.IMREAD_GRAYSCALE)

    image = np.dstack((
         r,
         g,
         b)
     )

    return image


def show_image(labels):
    """Show RGBY image with predicts in title

    Parameters
    ----------
    labels : list
        List with network predicts

    """

    args = get_args()

    # get title coordinates
    title_ordinate = -10
    title_abscissa = 205
    title_ordinate_sep = 0

    # reset seaborn style
    sns.reset_orig()

    # Create RGBY image
    red = np.array(Image.open(
        args.image_dir + '/' + args.image_id + '_red.png').convert("L"))
    green = np.array(Image.open(
        args.image_dir + '/' + args.image_id + '_green.png').convert("L"))
    blue = np.array(Image.open(
        args.image_dir + '/' + args.image_id + '_blue.png').convert("L"))
    yellow = np.array(Image.open(
        args.image_dir + '/' + args.image_id + '_yellow.png').convert("L"))

    demo_rgby = Image.fromarray(np.concatenate((np.expand_dims(
                                                red, axis=2),
                                                np.expand_dims(
                                                green, axis=2),
                                                np.expand_dims(
                                                blue, axis=2),
                                                np.expand_dims(
                                                yellow, axis=2)),
                                               axis=2))

    # Create title
    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(10, 10))

    for location in labels:
        ax.text(title_abscissa, title_ordinate + title_ordinate_sep,
                 subcell_locs[int(location)], fontsize=15)
        title_ordinate_sep -= 15

    ax.imshow(demo_rgby)

    # Remove coordinates axis
    ax.set_xticklabels([])
    ax.set_yticklabels([])
    ax.tick_params(left=False, bottom=False)

    # Save images
    plt.savefig(args.save_to +
                '/exmaple_' +
                str(len(os.listdir(args.save_to))))


def main():
    """Application entry point."""
    args = get_args()

    os.chdir(args.image_dir)

    path = os.path.join(args.image_dir, args.image_id)

    # Load image
    image = load_image(path)

    # Load model
    model = create_model()
    checkpoint = torch.load(args.model)
    model.load_state_dict(checkpoint['model_state_dict'])
    model.eval()
    ths = checkpoint['tresholds']

    # Transform image for model
    transform = albu.Compose([
        albu.Resize(*args.shape),
        albu.Normalize(mean=[0.485, 0.456, 0.406],
                       std=[0.229, 0.224, 0.225]),
        albupy.transforms.ToTensor()])
    image = transform(image=image)['image'].unsqueeze(axis=0)

    # Predicted
    output = model(image)

    label_predict = []

    # Save predicts
    for idx in range(28):
        if output[0][idx] >= ths[idx]:
            label_predict.append(idx)

    show_image(label_predict)
    print('Image saved')


if __name__ == '__main__':
    main()
