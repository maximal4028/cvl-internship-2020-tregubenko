# Human Protein Atlas Image Classification

## Getting started

Run `demo.py` on your data:
```bash
python3 demo/demo.py --model model/best_loss.pth --model-path data/model.pth --image-dir PATH_TO_FOLDER_WITH_IMAGES_ONLY --image-id IMAGE_NAME --save-to PATH_TO_FOLDER_WITH_RESULT_IMAGE  
```
## Initial problem

Classify subcellular protein patterns in human cells

## Data analysis

#### Analysis 

The source dataset contains 4 different channels of one image

Image Size: 512x512

- The source dataset size: 31072 train and 11702 test images
- The number of classes: 28
- Names of channels: red, green, blue, yellow
- Images can have several classes at the same time

Distribution of images by the number of classes:

![Figure_1.png](content/data_analysis_example/Figure_1.png?raw=true)

##### Problems in this dataset:

- Images have an additional channel
- Class imbalance 

#### Image examples

#### Different channels with labels

![exmaple_0.png](/content/data_analysis_example/exmaple_0.png?raw=true)

#### RGBY image

![exmaple_1.png](/content/demo_example/exmaple_1.png?raw=true)

## NN architecture

Pretrained ResNet34 for 28 classes

## Data preparation

- Divide the dataset into train/test
- Resize image to 224х224 for ResNet34
- Create RGB image 

## Training process

The resulted model was trained on the notebook with the following hardware:
- GPU: NVIDIA GeForce 1060
- CPU: Intel Core i7-8750H
- RAM: 16GB DDR4
- Storage: SSD

The training process was developed on the PyTorch. The training script 
contains here: `train/train_pytorch.py`

The training process features:
- Augmentation (RandomRotate90, Flip, VerticalFlip, HorizontalFlip, RandomBrightnessContrast)
- Finding best thresholds for each epoch (Calculating F1-score for each class)
- Saving models with best and current losses


#### History or training process

![train_process.png](/content/train_process/train_process.png?raw=true)

In the training process was used **focal loss** function.  


#### Final metrics

| Metrics    | Values |
|------------|--------|
| Precision  | 0.65   |
| Recall     | 0.673  |
| F1         | 0.658  |
| LB Public  | 0.357  |
| LB Private | 0.332  |

LB Public and LB Private - special kaggle competition leaderboard metrics.
LB Public is calculated with approximately 29% of the test data.
LB Private is calculated with approximately 71% of the test data.

#### Prediction example

`demo.py` file create RGBY image from 4 different channel images. Then model predict labels for this image.
And later, this labels printing in the image's title.

![exmaple_2.png](/content/demo_example/exmaple_2.png?raw=true) 

## Demonstration and quality evaluation

Script for the demonstration and quality evaluation contains here: 
`demo/demo.py`. It allows to run the model for one image.
Script for the quality evaluation contains here: 
`common/evaluation.py`.

## Plans and future improvements

- Training on the RGBY images
- Training on the full large external data (mix of 2048x2048 and 3072x3072 TIFF files. Size 250 GB). 
- Use learning rate scheduler
- Change network architecture